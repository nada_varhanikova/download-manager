//
// Created by Dominik-PC on 06/01/2021.
//
#ifndef DOWNLOADMANAGER_DOWNLOADHTTP_H
#define DOWNLOADMANAGER_DOWNLOADHTTP_H

#include "myData.h"
using namespace std;

class downloadHttp {
private:

public:
    downloadHttp(int a);
    int download(myData *data);
    int dajVelkostSuboru(int socket);

};


#endif //DOWNLOADMANAGER_DOWNLOADHTTP_H
