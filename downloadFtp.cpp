//
// Created by Dominik-PC on 07/01/2021.
//

#include "downloadFtp.h"
#include <sstream>

int downloadFtp::download(myData *dataz) {
    int sock, sockfd;
    char recvBuff[1024];
    char message[50]="" ;
    struct sockaddr_in server_addr, file_addr;
    struct hostent *server;


    server = gethostbyname(dataz->domena.c_str());
    if (server == NULL){
        herror("gethostbyname");
    }

    if( (sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("Socket");
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(21);
    server_addr.sin_addr = *((struct in_addr *)server->h_addr);
    //bzero(&(server_addr.sin_zero),8);
    printf("Connecting ...\n");

    if ( connect(sock, (struct sockaddr *)&server_addr,sizeof(struct sockaddr)) < 0)
        perror("Connect failed");
    printf("connected.\n");
    recv(sock,recvBuff,1024,0);
    printf("%s\n",recvBuff);

    strcpy(message, "USER ftp_student\r\n");
    send(sock, message, strlen(message), 0);
    recv(sock,recvBuff,1024,0);
    printf("%s\n",recvBuff);
    strcpy(message, "PASS st123ftp\r\n");
    send(sock, message, strlen(message), 0);
    recv(sock,recvBuff,1024,0);
    printf("%s\n",recvBuff);


    strcpy(message, "EPSV\r\n");
    send(sock, message, strlen(message), 0);
    recv(sock,recvBuff,1024,0);
    printf("%s\n",recvBuff);


    int p;
    sscanf(recvBuff,"229 Entering Extended Passive Mode (|||%d|)",&p);

    if( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("Socket");
    }
    file_addr.sin_family = AF_INET;
    file_addr.sin_port = htons(p);
    file_addr.sin_addr = *((struct in_addr *)server->h_addr);
    if ( connect(sockfd, (struct sockaddr *)&file_addr,sizeof(struct sockaddr)) < 0)
        perror("Connect failed");
    printf("connected.\n");
    strcpy(message, "TYPE I\r\n");
    send(sock, message, strlen(message), 0);
    recv(sock,recvBuff,1024,0);
    //printf("%s\n",recvBuff);

    string s2 = "SIZE /" + dataz->adresa + "\r\n";
    strcpy(message, s2.c_str());

    char a[1024] = "";
    if(send(sock, message, strlen(message), 0)>=0)
        recv(sock,a,1024,0);

    string str(a);
    string c1 = str.substr(0,str.find(" "));
    string c2 = str.substr(4,str.find("\n"));

    stringstream strr(c1);

    int bla = 0;
    int size = 0;
    strr >> bla;
    stringstream str2(c2);

    str2>>size;
  dataz->stop =0;

while(dataz->stop==0 && size>0) {
    string s3 = "RETR /" + dataz->adresa + "\r\n";
    strcpy(message, s3.c_str());
    if (send(sock, message, strlen(message), 0) >= 0)
        recv(sock, recvBuff, size, 0);

    char buf[1024];
    char *user = getenv("USER");
    string s(user);
    string s1 = "/home/" + s + "/DownloadManager/" + dataz->nazovSuboru.c_str();
    snprintf(buf, sizeof(buf), "%s", s1.c_str());

    FILE * file = fopen(buf, "wb");
    int bytes_received = 0;
    int bytes = 0;
    while (bytes_received = (recv(sockfd, recvBuff, 2, 0))) {
        fwrite(recvBuff, 1, bytes_received, file);
        printf("Saving data...\n\n");
        bytes += bytes_received;
        printf("Bytes recieved: %d from %d\n", bytes, size);
        sleep(2);
        if(dataz->paused==1){
         
       pthread_cond_wait(dataz->resume, dataz->mutexStahovanie);
            
        }
        if (bytes >= size) {
            cout << "Data saved" << endl;
               cout << "Zvoľte jednu z možností:" << endl;
                cout << "   1 pre stiahnutie" << endl;
                cout << "   2 pre správu adresárov" << endl;
                cout << "   3 pre načasovanie sťahovania" << endl;
                cout << "   0 pre ukončenie" << endl;
                dataz->stop = 1;
            break;
        }
        if(dataz->stop == 1) {
            strcpy(message, "PASV\r\n");
            send(sock, message, strlen(message), 0);
            recv(sock,recvBuff,1024,0);
            printf("%s\n",recvBuff);
            strcpy(message, "ABOR\r\n");
            if(send(sock, message, strlen(message), 0)>=0)
                recv(sock,recvBuff,1024,0);
            printf("%s",recvBuff);
            break;
        }
    }
    fclose(file);
}
if(size ==0){
    cout << "Prázdny súbor!"<<endl;
}
    strcpy(message, "QUIT\r\n");
    send(sock, message, strlen(message), 0);
    recv(sock,recvBuff,1024,0);
    printf("%s\n",recvBuff);


    close(sock);
    close(sockfd);


    return 0;
}
