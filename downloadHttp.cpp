//
// Created by Dominik-PC on 06/01/2021.
//
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <netdb.h>
#include <iostream>
#include <string.h>

downloadHttp::downloadHttp(int a) {}

int downloadHttp::download(myData *data) {
    cout << "" << data->domena << ""  << endl;
    char * path = const_cast<char*>(data->adresa.c_str());

    int sock, bytes_received;
    char send_data[1024]={"0"};
    char recv_data[1024], *p;

    struct sockaddr_in server_addr;
    struct hostent *server;

    server = gethostbyname(data->domena.c_str());
    if (server == NULL){
        herror("gethostbyname");
    }

    if( (sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("Socket");
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(80);
    server_addr.sin_addr = *((struct in_addr *)server->h_addr);
    bzero(&(server_addr.sin_zero),8);

    printf("Connecting ...\n");

    if ( connect(sock, (struct sockaddr *)&server_addr,sizeof(struct sockaddr)) < 0){
        perror("Connect failed");
    }

    snprintf(send_data, sizeof(send_data), "GET /%s HTTP/1.1\r\nHost: %s\r\n\r\n", path, data->domena.c_str());
    data->stop = 0;
    while(data->stop == 0) {
        if( (send(sock, send_data, strlen(send_data), 0)) < 0){
            perror("send");
        }

        int contentlengh;
        int bytes=0;
        contentlengh = dajVelkostSuboru(sock);

        if(contentlengh > 0) {
            cout << "začiatok sťahovania"<<endl;

            char buf[1024];
        char * user = getenv("USER");
        string s(user);
        string s1 = "/home/" + s + "/DownloadManager/" + data->nazovSuboru.c_str();
            snprintf(buf, sizeof(buf), "%s",s1.c_str());
            FILE *fd = fopen(buf, "wb");

            while (bytes_received = (recv(sock, recv_data, 2048, 0)) ) {
                if (bytes_received == -1) {
                    perror("recieve");
                    exit(3);
                    break;
                }

                while(data->paused == 1) {
                    pthread_cond_wait(data->resume, data->mutexStahovanie);
                }
                fwrite(recv_data, 1, bytes_received, fd);
                printf("Saving data...\n\n");
                bytes += bytes_received;
                printf("Bytes recieved: %d from %d\n", bytes, contentlengh);
                sleep(2);

                if (bytes >= contentlengh) {
                    data->stop = 1;
                    cout << "Data saved"<<endl;
                    cout << "Zvoľte jednu z možností:" << endl;
                cout << "   1 pre stiahnutie" << endl;
                cout << "   2 pre správu adresárov" << endl;
                cout << "   3 pre načasovanie sťahovania" << endl;
                cout << "   0 pre ukončenie" << endl;
                    break;
                }
                if(data->stop == 1) {
                    cout << "Zastavenie sťahovania"<<endl;
                    break;
                }
            }
            fclose(fd);
        } else {
            data->stop=1;

        }
    }
    data->stop = 0;
    return 0;
}

int downloadHttp::dajVelkostSuboru(int socket) {
    char buff[1024]="",*ptr=buff+4;
    int bytes_received, status;
    printf("Begin HEADER ..\n");
    while(bytes_received = recv(socket, ptr, 1, 0)){

        if(bytes_received==-1 ){
            printf("nie je možné stiahnuť tento obrázok");
            perror("Parse Header");
            exit(1);
        }
        if(
                (ptr[-3]=='\r')  && (ptr[-2]=='\n' ) &&
                (ptr[-1]=='\r')  && (*ptr=='\n' )
                ) break;
        ptr++;

    }

    *ptr=0;

    if(bytes_received) {
        ptr = strstr(buff+4, "Content-Length:");
        if (ptr) {
            sscanf(ptr, "%*s %d", &bytes_received);
        } else {
            ptr = strstr(buff+4, "content-length:");
            if(ptr) {
                sscanf(ptr, "%*s %d", &bytes_received);
            } else {
                cout << "Nie je možné stiahnuť daný súbor! "<<endl;
                return -1;
            }
        }

    }

    printf("End HEADER ..\n");
    return  bytes_received ;

};

