//
// Created by Dominik-PC on 06/01/2021.
//

#include <string>
#include <iostream>
#include "myData.h"
using namespace std;

myData::myData(pthread_mutex_t* mut, pthread_cond_t* cond) {
    this->nazovSuboru = "";
    this->domena = "";
    this->adresa = "";
    this->token = "";
    this->hod=0;
    this->min=0;
    this->casovanie=false;
    this->stop = 0;
    this->paused = 0;
    this->mutexStahovanie = mut;
    this->resume = cond;
}

void myData::rozdelUrlAdresu(string urlAdresa) {
    this->token = urlAdresa.substr(0, urlAdresa.find("//"));

    string withoutToken = urlAdresa.substr(token.length() + 2, urlAdresa.length());
    string someString = withoutToken.substr(0, withoutToken.find("/"));

    this->domena = const_cast<char *>(someString.c_str());
    this->adresa = withoutToken.substr(someString.length() + 1, withoutToken.length());
}

void myData::nacitajData() {
    string url;
    string nazov;

    printf("Zadajte adresu: ");
    cin >> url;
    rozdelUrlAdresu(url);
    printf("Zadajte názov pre stiahnuty subor: \n");
    cin >> nazov;
    this->nazovSuboru =&nazov[0];
}

string myData::getToken() {
    return this->token;
}
