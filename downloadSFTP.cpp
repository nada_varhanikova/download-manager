
#include <iostream>
#include <libssh2.h>
#include <libssh2_sftp.h>

#include <string>
#include <ctime>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>
#include <netinet/in.h>
#include <netdb.h>

#define STORAGE "/home/pall1/DownloadManager/abc"

using namespace std;

static int waitsocket(int socket_fd, LIBSSH2_SESSION *session)
{
    struct timeval timeout;
    int rc;
    fd_set fd;
    fd_set *writefd = NULL;
    fd_set *readfd = NULL;
    int dir;

    timeout.tv_sec = 10;
    timeout.tv_usec = 0;

    FD_ZERO(&fd);

    FD_SET(socket_fd, &fd);

    dir = libssh2_session_block_directions(session);

    if(dir & LIBSSH2_SESSION_BLOCK_INBOUND)
        readfd = &fd;

    if(dir & LIBSSH2_SESSION_BLOCK_OUTBOUND)
        writefd = &fd;

    rc = select(socket_fd + 1, readfd, writefd, NULL, &timeout);

    return rc;
}

int downloadSFTP() {
    int sock, i, auth_pw = 1;
    struct sockaddr_in sin;
    const char *fingerprint;
    LIBSSH2_SESSION *session;
    const char *username = "pall1";
    const char *password = ""; //Tvoje heslo
    const char *sftppath = "/home/pall1/pos/abc.txt"; //Cesta odkial

    int rc;
    LIBSSH2_SFTP *sftp_session;
    LIBSSH2_SFTP_HANDLE *sftp_handle;
    FILE *tempstorage;
    char mem[1000];
    struct timeval timeout;
    fd_set fd;
    fd_set fd2;
    size_t nread;
    char *ptr;

    struct hostent *server;
    server = gethostbyname("frios2.fri.uniza.sk");
    rc = libssh2_init(0);
    if(rc != 0) {
        fprintf(stderr, "libssh2 initialization failed (%d)\n", rc);
        return 1;
    }

    sock = socket(AF_INET, SOCK_STREAM, 0);

    sin.sin_family = AF_INET;
    sin.sin_port = htons(10022);
    sin.sin_addr = *((struct in_addr *)server->h_addr);
    if(connect(sock, (struct sockaddr*)(&sin),
               sizeof(struct sockaddr_in)) != 0) {
        fprintf(stderr, "failed to connect!\n");
        return -1;
    }

    session = libssh2_session_init();
    if(!session)
        return -1;

    libssh2_session_set_blocking(session, 1);

    rc = libssh2_session_handshake(session, sock);

    if(rc) {
        fprintf(stderr, "Failure establishing SSH session: %d\n", rc);
        return -1;
    }


    if(auth_pw) {
        while((rc = libssh2_userauth_password(session, username, password))
              == LIBSSH2_ERROR_EAGAIN);
        if(rc) {
            fprintf(stderr, "Authentication by password failed.\n");
        }
    }

    do {
        sftp_session = libssh2_sftp_init(session);


        if(!sftp_session) {
            if(libssh2_session_last_errno(session) ==

               LIBSSH2_ERROR_EAGAIN) {
                fprintf(stderr, "non-blocking init\n");
                waitsocket(sock, session); /* now we wait */
            }
            else {
                fprintf(stderr, "Unable to init SFTP session\n");
            }
        }
    } while(!sftp_session);

    /* Request a file via SFTP */
    do {
        sftp_handle = libssh2_sftp_open(sftp_session, sftppath,

                                        LIBSSH2_FXF_READ, 0);

        if(!sftp_handle) {
            if(libssh2_session_last_errno(session) != LIBSSH2_ERROR_EAGAIN) {

                fprintf(stderr, "Unable to open file with SFTP\n");
            }
            else {
                fprintf(stderr, "non-blocking open\n");
                waitsocket(sock, session);
            }
        }
    } while(!sftp_handle);

    tempstorage = fopen("/home/pall1/DownloadManager/abc.txt", "wb");
    do {
        do {
            rc = libssh2_sftp_read(sftp_handle, mem, sizeof(mem));

            fprintf(stderr, "libssh2_sftp_read returned %d\n",
                    rc);

            if(rc > 0) {
                write(2, mem, rc);
                fwrite(mem, rc, 1, tempstorage);
            }
        } while(rc > 0);

        if(rc != LIBSSH2_ERROR_EAGAIN) {
            break;
        }

        timeout.tv_sec = 10;
        timeout.tv_usec = 0;

        FD_ZERO(&fd);
        FD_ZERO(&fd2);
        FD_SET(sock, &fd);
        FD_SET(sock, &fd2);

        rc = select(sock + 1, &fd, &fd2, NULL, &timeout);
        if(rc <= 0) {
            fprintf(stderr, "SFTP download timed out: %d\n", rc);
            break;
        }

    } while(1);
    libssh2_sftp_close(sftp_handle);
    fclose(tempstorage);
    return 0;
}
