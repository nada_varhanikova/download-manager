//
// Created by Dominik-PC on 06/01/2021.
//

#include <string>
#ifndef DOWNLOADMANAGER_DATA_H
#define DOWNLOADMANAGER_DATA_H

using namespace std;
class myData {
private:
public:
    string nazovSuboru;
    string domena;
    string token;
    string adresa;
    int hod;
    int min;
    bool casovanie;
    int stop;
    int paused;
    
    pthread_mutex_t* mutexStahovanie;
    pthread_cond_t* resume;
    

    myData(pthread_mutex_t* mut, pthread_cond_t* cond);
    void rozdelUrlAdresu(string urlAdresa);
    void nacitajData();
    string getToken();
};

#endif //DOWNLOADMANAGER_DATA_H
